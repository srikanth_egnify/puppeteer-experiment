const puppeteer = require('puppeteer');
const fs = require('fs');

const makeOptionsMarkup = (qOptions) => {
  let markup = `<div class="options">`;
  for (let i = 0; i < qOptions.length; i += 2) {
    markup += `<div class="options-row">`;
    const firstLabel = String.fromCharCode(65 + i);
    const firstOption = (qOptions[i] + '')
                          .replace('<title></title>', '')
                          .replace('<p>', '')
                          .replace('</p>', '');
    const secondLabel = String.fromCharCode(65 + i + 1);
    const secondOption = (qOptions[i+1] + '')
                          .replace('<title></title>', '')
                          .replace('<p>', '')
                          .replace('</p>', '');
    markup += `<div class="opt-${i}">${firstLabel}) ${firstOption}</div>`;
    markup += `<div class="opt-${i+1}">${secondLabel}) ${secondOption}</div>`;
    markup += `</div>`
  }
  markup += `</div>`;
  return markup;
};

const readQuestions = () => {
  let templateHtml = fs.readFileSync(__dirname + '/template.html').toString('utf8');
  const qList = [];
  let qCount = 1;

  const questions = JSON.parse(fs.readFileSync('db.json'));
  questions.forEach(([_, subjectQuestions]) => {
    subjectQuestions.forEach(([_, sectionQuestions]) => {
      sectionQuestions.forEach(question => {

        const optionsMarkup = makeOptionsMarkup(question.options);
        
        const markup = templateHtml
                          .replace("#*QNO*#", `Q${qCount})`)
                          .replace("#*QCONTENT*#", question.question)
                          .replace("#*OPTIONS*#", optionsMarkup);
        question.markup = markup;
        qCount += 1;
        qList.push(question);
      });
    });
  });
  return qList;
};

const qString = `
<div class="question-row">
  <div class="ql">
    <b>#*QNO*#</b>.
  </div>
  <div class="q-content">
    <div class='blue-column'>
    #*QCONTENT*#
    #*OPTIONS*#
    </div>
  </div>
</div>`;

const styleString = `
<style>

  img {
    vertical-align: middle;
  }

  page {
    display: block;
    margin: 0 auto;
    margin-bottom: 0.5cm;
    background-image: url('https://storage.googleapis.com/vega-demo-cdn/getranks_trans.png');
    background-repeat: repeat-y;
    background-position: center;
  }

  page[size="A4"] {
    width: 21cm;
    height: 29.7cm;
  }

  .l {
    width: 10.5cm;
    height: 29.7cm;
    float: left;
  }

  .r {
    width: 10.5cm;
    height: 29.7cm;
    float: right;
  }

  .question-row {
    display: flex;
  }

  .ql {
    width: 1cm;
    float: left;
  }

  .q-content {
    width: 9.5cm;
    height: auto;
    float: right;
  }

  .options {
    display: flex;
    flex-flow: column wrap;
    justify-content:flex-start;
    flex-direction: row;
  }

  .options-row {
    padding:0 70px 0 0;
    line-height: 40px;;
  }
  p {
    
  }
</style>`;

const makePages = (dimensions) => {
  const pages = [];
  const columns = [];
  let column = [];
  let currentHeight = 0;
  dimensions.forEach(([width, height]) => {
    if (currentHeight + height < 1120) {
      column.push([width, height]);
    } else {
      columns.push(column);
      column = [[width, height]];
      currentHeight = 0;
    }
    currentHeight += height;
  });
  columns.push(column);
  for (let i = 0; i < columns.length; i += 2) {
    const leftColumn = columns[i] ? columns[i] : [];
    const rightColumn = columns[i+1] ? columns[i+1] : [];
    pages.push([leftColumn, rightColumn]);
  }
  
  return pages;
};

(async () => {
  // let templateHtml = fs.readFileSync(__dirname + '/template.html').toString('utf8');

  const browser = await puppeteer.launch();
  const page = await browser.newPage();
  const questions = readQuestions();
  const dimensions = [];

  for (var i = 0; i < questions.length; i++) {
    const markup = questions[i].markup;
    await page.setContent(markup);

    const result = await page.evaluate(() => {
      const b = document.getElementsByClassName('question-row')[0];
      return [b.offsetWidth, b.offsetHeight];
    });
    dimensions.push(result);
  }



  const pages = makePages(dimensions);

  // console.log(pages);

  let paperMarkup = `<html>${styleString}`;
  let qcnt = 0;
  pages.forEach(([leftColumn, rightColumn]) => {
    paperMarkup = paperMarkup + `
    <page size="A4" class="page-1">
    <div class="content">
        <div class="l">`;
    
    leftColumn.forEach(_ => {
      const currentQ = questions[qcnt];

      const qno = `Q${qcnt + 1}`;
      const qcontent = currentQ.question;
      const optionsMarkup = makeOptionsMarkup(currentQ.options);
      let questionMarkup = qString
                            .replace("#*QNO*#", qno)
                            .replace("#*QCONTENT*#", qcontent)
                            .replace("#*OPTIONS*#", optionsMarkup);
      paperMarkup += questionMarkup;
      qcnt++;
    });
    paperMarkup += '</div>';
    paperMarkup += `<div class="r">`;
    rightColumn.forEach(_ => {
      const currentQ = questions[qcnt];

      const qno = `Q${qcnt + 1}`;
      const qcontent = currentQ.question;
      const optionsMarkup = makeOptionsMarkup(currentQ.options);

      let questionMarkup = qString
                            .replace("#*QNO*#", qno)
                            .replace("#*QCONTENT*#", qcontent)
                            .replace("#*OPTIONS*#", optionsMarkup);
      paperMarkup += questionMarkup;
      qcnt++;
    });
    paperMarkup += `</div></div>`;
    paperMarkup += '</page>';
  });
  paperMarkup += `</html>`;

  await page.setContent(paperMarkup);

  await page.pdf({ path: 'file.pdf', format: 'A4', printBackground: true});

  console.log(paperMarkup)

  await browser.close();
})()